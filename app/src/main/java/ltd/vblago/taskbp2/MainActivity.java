package ltd.vblago.taskbp2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import ltd.vblago.taskbp2.model.Colors;
import ltd.vblago.taskbp2.util.ColorGenerator;

public class MainActivity extends AppCompatActivity {

    ArrayList<View> views;

    Colors colors;

    String[] saveColors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        views = new ArrayList<>();
        views.add(findViewById(R.id.one));
        views.add(findViewById(R.id.two));
        views.add(findViewById(R.id.three));
        views.add(findViewById(R.id.four));

        colors = new Colors(ColorGenerator.generateColors());

        saveColors = new String[4];

        setBackgroundColors(colors.randomColors(""), -1);

        views.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundColors(colors.randomColors(saveColors[0]), 0);
            }
        });

        views.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundColors(colors.randomColors(saveColors[1]), 1);
            }
        });

        views.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundColors(colors.randomColors(saveColors[2]), 2);
            }
        });

        views.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBackgroundColors(colors.randomColors(saveColors[3]), 3);
            }
        });
    }

    private void setBackgroundColors(Set<String> set, int num) {
        Iterator<String> iterator = set.iterator();
        for (int i = 0; i < 4; i++) {
            if (i != num) {
                saveColors[i] = iterator.next();
                views.get(i).setBackgroundColor(Color.parseColor(saveColors[i]));
            }
        }
    }
}
