package ltd.vblago.taskbp2.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Colors {

    private ArrayList<String> colors;

    public Colors(ArrayList<String> colors) {
        this.colors = colors;
    }

    public Set<String> randomColors(String busy) {
        Set<String> set = new HashSet<>();
        while (set.size() != 4) {
            int rand = (int) (Math.random() * 10);
            String color = colors.get(rand);
            if (!color.equals(busy)){
                set.add(color);
            }
        }
        return set;
    }
}
