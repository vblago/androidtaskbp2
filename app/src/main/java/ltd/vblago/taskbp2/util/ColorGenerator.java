package ltd.vblago.taskbp2.util;

import java.util.ArrayList;

public class ColorGenerator {

    public static ArrayList<String> generateColors(){
        ArrayList<String> colors = new ArrayList<>();
        colors.add("#708090");
        colors.add("#0000CD");
        colors.add("#006400");
        colors.add("#98FB98");
        colors.add("#BDB76B");
        colors.add("#FFD700");
        colors.add("#BC8F8F");
        colors.add("#B22222");
        colors.add("#FFDAB9");
        colors.add("#8B1C62");

        return colors;
    }
}
